<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoice
 *
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvoiceRepository")
 */
class Invoice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="invoiceDate", type="date")
     */
    private $invoiceDate;

    /**
     * @var int
     *
     * @ORM\Column(name="invoiceNumber", type="integer")
     */
    private $invoiceNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="customerId", type="integer")
     */
    private $customerId;



    /**
     * @ORM\OneToMany(targetEntity="InvoiceRow", mappedBy="invoice", cascade={"persist" , "remove"})
     */
    private $InvoiceRow;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set invoiceDate
     *
     * @param \DateTime $invoiceDate
     *
     * @return Invoice
     */
    public function setInvoiceDate($invoiceDate)
    {
        $this->invoiceDate = $invoiceDate;

        return $this;
    }

    /**
     * Get invoiceDate
     *
     * @return \DateTime
     */
    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    /**
     * Set invoiceNumber
     *
     * @param integer $invoiceNumber
     *
     * @return Invoice
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }

    /**
     * Get invoiceNumber
     *
     * @return int
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return Invoice
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->InvoiceRow = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add InvoiceRow
     *
     * @param \AppBundle\Entity\InvoiceRow $InvoiceRow
     *
     * @return Invoice
     */
    public function addInvoiceRow(\AppBundle\Entity\InvoiceRow $InvoiceRow)
    {
        $this->InvoiceRow[] = $InvoiceRow;
        // setting the current user to the $InvoiceRow,
        // adapt this to whatever you are trying to achieve
        $InvoiceRow->setInvoice($this);
        return $this;
    }

    /**
     * Remove InvoiceRow
     *
     * @param \AppBundle\Entity\InvoiceRow $InvoiceRow
     */
    public function removeInvoiceRow(\AppBundle\Entity\InvoiceRow $InvoiceRow)
    {
        $this->InvoiceRow->removeElement($InvoiceRow);
    }

    /**
     * Get InvoiceRow
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoiceRow()
    {
        return $this->InvoiceRow;
    }
}
