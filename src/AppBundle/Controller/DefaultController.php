<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\InvoiceRow;
use AppBundle\Form\InvoiceType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
      $invoices= $this->getDoctrine()
      ->getRepository('AppBundle:Invoice')
      ->findAll();

      return $this->render('invoice/index.html.twig', array('invoices' => $invoices ));
    }


    /**
   * @Route("details/{id}", name="details")
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function detailsAction(Request $request, $id)
  {
      $em = $this->getDoctrine()->getManager();
      /**
       * @var $invoice Invoice
       */
      $invoice = $em->getRepository('AppBundle:Invoice')->findOneBy(['id' => $id]);

      // save the records that are in the database first to compare them with the new one the user sent
      // make sure this line comes before the $form->handleRequest();
      $orignalRow = new ArrayCollection();
      foreach ($invoice->getInvoiceRow() as $row) {
          $orignalRow->add($row);
      }

      $form = $this->createForm(InvoiceType::class, $invoice);
      $form->handleRequest($request);

      if ($form->isSubmitted()) {
          // get rid of the ones that the user got rid of in the interface (DOM)

        foreach ($orignalRow as $row) {
              // check if the row is in the $invoice->getInvoiceRow()
              if ($invoice->getInvoiceRow()->contains($row) === false) {
                  $em->remove($row);
              }
          }
          $em->persist($invoice);
          $em->flush();
      }

      // replace this example code with whatever you need
      return $this->render('invoice/details.html.twig', [
          'form' => $form->createView()
      ]);
  }

  /**
  *
  * @Route("/delete/{id}", name="invoice_delete")
  */
  public function deleteInvoiceAction($id, Request $request){
      $em= $this->getDoctrine()->getManager();
      $invoice = $em->getRepository('AppBundle:Invoice')->find($id);
      $invoice->getInvoiceRow()->removeElement($id);
      $em->remove($invoice);
      $em->flush();

      $this->addFlash('notice','invoice removed');
      return $this->redirectToRoute('homepage');
  }

}
