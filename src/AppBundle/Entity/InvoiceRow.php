<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoiceRow
 *
* @ORM\Table(name="InvoiceRow")
* @ORM\Entity(repositoryClass="AppBundle\Repository\InvoiceRowRepository")
*/
class InvoiceRow
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
    private $id;

    /**
 * @var string
 *
 * @ORM\Column(name="description", type="text")
 */
private $description;

/**
 * @var int
 *
 * @ORM\Column(name="quantity", type="integer")
 */
private $quantity;

/**
 * @var string
 *
 * @ORM\Column(name="amount", type="decimal", precision=12, scale=2)
 */
private $amount;

/**
 * @var string
 *
 * @ORM\Column(name="vatAmount", type="decimal", precision=12, scale=2)
 */
private $vatAmount;

/**
 * @var string
 *
 * @ORM\Column(name="totalWithVat", type="decimal", precision=12, scale=2)
 */
private $totalWithVat;

    /**
     * @ORM\ManyToOne(targetEntity="Invoice", inversedBy="InvoiceRow")
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $invoice;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InvoiceRow
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return InvoiceRow
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return InvoiceRow
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set vatAmount
     *
     * @param string $vatAmount
     *
     * @return InvoiceRow
     */
    public function setVatAmount($vatAmount)
    {
        $this->vatAmount = $vatAmount;

        return $this;
    }

    /**
     * Get vatAmount
     *
     * @return string
     */
    public function getVatAmount()
    {
        return $this->vatAmount;
    }

    /**
     * Set totalWithVat
     *
     * @param string $totalWithVat
     *
     * @return InvoiceRow
     */
    public function setTotalWithVat($totalWithVat)
    {
        $this->totalWithVat = $totalWithVat;

        return $this;
    }

    /**
     * Get totalWithVat
     *
     * @return string
     */
    public function getTotalWithVat()
    {
        return $this->totalWithVat;
    }

    /**
     * Set invoice
     *
     * @param \AppBundle\Entity\Invoice $invoice
     *
     * @return InvoiceRow
     */
    public function setInvoice(\AppBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \AppBundle\Entity\Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }
}
