<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* invoice/index.html.twig */
class __TwigTemplate_f74a3769fa33078c05f4231f0362ac9eb8b7c7617ac5dc95c1cf5f32b320c085 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "invoice/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "invoice/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "invoice/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"container\">

 ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "flashes", [0 => "notice"], "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 8
            echo " <div class=\"alert alert-success\" role=\"alert\">  ";
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo " </div>
 ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo " <h2>Invoices List</h2>
 <p> Crud operations of Invoices. Click on details to see all invoice rows. </p>
 <table class=\"table\">
   <thead>
     <tr>
       <th>Invoice ID</th>
       <th>Invoice date</th>
       <th>Invoice number</th>
       <th>Customer ID </th>
       <th></th>
     </tr>
   </thead>
   <tbody>
   ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["invoices"] ?? $this->getContext($context, "invoices")));
        foreach ($context['_seq'] as $context["_key"] => $context["invoice"]) {
            // line 24
            echo "     <tr>
       <th> ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["invoice"], "id", []), "html", null, true);
            echo "  </th>
       <th> ";
            // line 26
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["invoice"], "invoicedate", []), "F j, Y, g:i a"), "html", null, true);
            echo " </th>
       <th> ";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["invoice"], "invoicenumber", []), "html", null, true);
            echo " </th>
       <th> ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["invoice"], "customerid", []), "html", null, true);
            echo " </th>
       <th>
       <a href=\"/details/";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["invoice"], "id", []), "html", null, true);
            echo "\" class=\"btn btn-primary\">Details</a>
       <a href=\"/delete/";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["invoice"], "id", []), "html", null, true);
            echo "\" class=\"btn btn-danger delete-invoice\" data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["invoice"], "id", []), "html", null, true);
            echo "\">Delate</a>
       </th>
     </tr>
   ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['invoice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "   </tbody>
 </table>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 41
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        echo " <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script> ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "invoice/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 41,  134 => 35,  122 => 31,  118 => 30,  113 => 28,  109 => 27,  105 => 26,  101 => 25,  98 => 24,  94 => 23,  79 => 10,  70 => 8,  66 => 7,  61 => 4,  52 => 3,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

<div class=\"container\">

 {% for message in app.flashes('notice') %}
 <div class=\"alert alert-success\" role=\"alert\">  {{ message }} </div>
 {% endfor %}
 <h2>Invoices List</h2>
 <p> Crud operations of Invoices. Click on details to see all invoice rows. </p>
 <table class=\"table\">
   <thead>
     <tr>
       <th>Invoice ID</th>
       <th>Invoice date</th>
       <th>Invoice number</th>
       <th>Customer ID </th>
       <th></th>
     </tr>
   </thead>
   <tbody>
   {% for invoice in invoices %}
     <tr>
       <th> {{ invoice.id}}  </th>
       <th> {{ invoice.invoicedate|date('F j, Y, g:i a')}} </th>
       <th> {{ invoice.invoicenumber }} </th>
       <th> {{ invoice.customerid }} </th>
       <th>
       <a href=\"/details/{{ invoice.id }}\" class=\"btn btn-primary\">Details</a>
       <a href=\"/delete/{{ invoice.id }}\" class=\"btn btn-danger delete-invoice\" data-id=\"{{ invoice.id }}\">Delate</a>
       </th>
     </tr>
   {% endfor %}
   </tbody>
 </table>
</div>

{% endblock %}

{% block javascripts %} <script src=\"{{ asset('js/main.js') }}\"></script> {% endblock %}
", "invoice/index.html.twig", "C:\\xampp\\htdocs\\invoice\\app\\Resources\\views\\invoice\\index.html.twig");
    }
}
